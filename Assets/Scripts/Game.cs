﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Audio;

public enum EnemyType
{
	A,
	B,
	C,
	D,
	E,
	F,
	G,
	H,
	I,
	J,
	K,
	L
}

public enum HeroType
{
	hero1,
	hero2,
	hero3,
	hero4,
	hero5,
	hero6
}

[System.Serializable]
public class Monster
{
	public Monster(EnemyType t)
	{
		type = t;
	}

	[ReadOnly]
	public EnemyType type;
	public Sprite icon;
	public float centerY;
	public AudioClip spawnAudioClip;
}

[System.Serializable]
public class Hero
{
	public Hero(HeroType t)
	{
		type = t;
	}

	public string name;
	[ReadOnly]
	public HeroType type;
	public Sprite icon;
	public Font font;
	public Color colorFont;
	public float centerY;

	public List<EnemyType> winAgainst;
	public List<AudioClip> catchPhrases = new List<AudioClip>();
}

public class Game : MonoBehaviour {

	[SerializeField] TextMesh timer;
	[SerializeField] TextMesh score;
	public Transform limitDestroy;

	[Header("Monsters")]
	[SerializeField] Vector3 monsterDir;
	[SerializeField] EnemyUI monsterPrefab;
	[SerializeField] List<Monster> monsterList = new List<Monster>();

	[Header("Hero")]
	[SerializeField] Vector3 heroDir;
	[SerializeField] HeroUI heroPrefab;
	[SerializeField] List<Hero> heroList = new List<Hero>();

	[Header("Speed")]
	[SerializeField] float baseSpeed;
	[SerializeField] float incrementSpeed;

	[Header("Time")]
	[SerializeField] float gameDuration;
	[SerializeField] float enemyBaseSpawnRate;
	[SerializeField] float enemyMaxSpawnRate;
	[SerializeField] float enemyIncrementSpawnRate;

	[Space(10)]

	[SerializeField] Transform enemySpawnPoint;
	[SerializeField] Transform heroSpawnPoint;
	[SerializeField] Transform enemyGoalPoint;

	[Space(10)]

	[SerializeField] float bufferTime = 1;

	[Header("Difficulty")]
	[SerializeField] float onePlayerDifficultyRatio;
	[SerializeField] float twoPlayerDifficultyRatio;
	[SerializeField] float threePlayerDifficultyRatio;
	[SerializeField] float fourPlayerDifficultyRatio;

	[Header("Animation")]
	[SerializeField] Animator heroShowAnimator;
	[SerializeField] SpriteRenderer heroShowBackground;
	[SerializeField] SpriteRenderer heroShowSprite;
	[SerializeField] TextMesh heroShowText;
	[SerializeField] AnimationClip heroShowAnimation;
	[SerializeField] AnimationClip heroShowIddleAnimation;

	[Header("Sound")]
	[SerializeField] SoundManager soundManager;
	[SerializeField] AudioMixer musicMixer;
	[SerializeField] float musicVolumeDifferenceAtPhrase;
	[SerializeField] float musicFadeDuration;
	
	float initialMusicVolume;

	private float _showDuration;

	private int playerNumber;
	private float _score = 0;
	[HideInInspector]
	public bool Paused = false;

	[Header("Jones Names")]
	public List<string> jonesNames = new List<string>();

	[Header("Particles")]
	[SerializeField] ParticleSystem starsParticles;
	[SerializeField] Vector2 minMaxStarParticles;

	public void spawnStars(Vector3 pos)
	{
		starsParticles.transform.position = pos;
		starsParticles.Emit(Random.Range((int)minMaxStarParticles.x, (int)minMaxStarParticles.y));
	}

	void GenerateJonesName()
	{
		Hero jones = getHero(HeroType.hero2);
		jones.name = jonesNames[Random.Range(0, jonesNames.Count)] + "Jones";
	}

	public float SpawnRate
	{
		get { return Mathf.Max(enemyMaxSpawnRate, (enemyBaseSpawnRate - enemyIncrementSpawnRate * _gameTime) * DifficultyRatio); }
	}

	public float DifficultyRatio
	{
		get
		{
			switch(playerNumber)
			{
				case 1:return onePlayerDifficultyRatio;
				case 2: return twoPlayerDifficultyRatio;
				case 3: return threePlayerDifficultyRatio;
				case 4: return fourPlayerDifficultyRatio;

				default: return 1;
			} 
		}
	}

	public float CurrentSpeed
	{
		get
		{
			return (baseSpeed + incrementSpeed * _gameTime) * DifficultyRatio ;
		}
	}

	private float _gameTime = 0;

	private bool _stopSpawn;

	private List<HeroType> _heroStock = new List<HeroType>();
	private HeroUI _currentHeros = null;
	private Dictionary<HeroType, float> _heroBuffer = new Dictionary<HeroType, float>();
	private float _launchBuffer = -10;

	private void Start()
	{
		GenerateJonesName();
		heroShowBackground.enabled = false;
		_showDuration = heroShowAnimation.length + heroShowIddleAnimation.length;
		playerNumber = PlayerPrefs.GetInt("playerNumber");

		EnemyUI.StartPoint = enemySpawnPoint.position;
		EnemyUI.EndPoint = enemyGoalPoint.position;
		EnemyUI.DistanceEndStart = (enemySpawnPoint.position - enemyGoalPoint.position).magnitude;
		EnemyUI.SoundManagerInstance = soundManager;

		Vector3 tmp = EnemyUI.EndPoint - EnemyUI.StartPoint;
		Debug.DrawLine(EnemyUI.StartPoint, EnemyUI.StartPoint + tmp * 0.3333f, Color.green, 30);
		Debug.DrawLine(EnemyUI.StartPoint + tmp * 0.3333f, EnemyUI.StartPoint + tmp * 0.6666f, Color.yellow, 30);
		Debug.DrawLine(EnemyUI.StartPoint + tmp * 0.66666f, EnemyUI.StartPoint + tmp, Color.red, 30);

		foreach (HeroType h in System.Enum.GetValues(typeof(HeroType)))
		{
			_heroBuffer.Add(h, Time.time - 10);
		}

		CameraInit();

		StartCoroutine(GameCoroutine());
	}

	void CameraInit()
	{
		// set the desired aspect ratio (the values in this example are
		// hard-coded for 16:9, but you could make them into public
		// variables instead so you can set them at design time)
		float targetaspect = 16.0f / 9.0f;

		// determine the game window's current aspect ratio
		float windowaspect = (float)Screen.width / (float)Screen.height;

		// current viewport height should be scaled by this amount
		float scaleheight = windowaspect / targetaspect;

		// obtain camera component so we can modify its viewport
		Camera camera = Camera.main;

		// if scaled height is less than current height, add letterbox
		if (scaleheight < 1.0f)
		{
			Rect rect = camera.rect;

			rect.width = 1.0f;
			rect.height = scaleheight;
			rect.x = 0;
			rect.y = (1.0f - scaleheight) / 2.0f;

			camera.rect = rect;
		}
		else // add pillarbox
		{
			float scalewidth = 1.0f / scaleheight;

			Rect rect = camera.rect;

			rect.width = scalewidth;
			rect.height = 1.0f;
			rect.x = (1.0f - scalewidth) / 2.0f;
			rect.y = 0;

			camera.rect = rect;
		}
	}

	private void Update()
	{
		score.text = string.Format("{0:000000000}", (int)_score);
	}

	IEnumerator GameCoroutine()
	{
		StartCoroutine(SpawnEnemiesCoroutine());
		float startGame = Time.time;

		while(true)
		{
			_gameTime = Time.time - startGame;

			float minutes = Mathf.Floor(_gameTime / 60); float seconds = Mathf.RoundToInt(_gameTime % 60);
			timer.text = string.Format("{0:00}:{1:00}", minutes, seconds);

			GetInputs();

			yield return null;
		}
	}

	IEnumerator SpawnEnemiesCoroutine()
	{
		_stopSpawn = false;

		while (!_stopSpawn)
		{
			spawnEnemy();

			float start = Time.time;
			while (Time.time - start < SpawnRate)
				yield return null;
		}
	}

	void GetInputs()
	{
		if (!Paused)
		{
			if (Input.GetKey(KeyCode.Alpha1) || Input.GetKey(KeyCode.Keypad1) || Input.GetKey(KeyCode.UpArrow))
				AddHeroToStock(HeroType.hero1);

			if (Input.GetKey(KeyCode.Alpha2) || Input.GetKey(KeyCode.Keypad2) || Input.GetKey(KeyCode.LeftArrow))
				AddHeroToStock(HeroType.hero2);

			if (Input.GetKey(KeyCode.Alpha3) || Input.GetKey(KeyCode.Keypad3) || Input.GetKey(KeyCode.G))
				AddHeroToStock(HeroType.hero3);

			if (Input.GetKey(KeyCode.Alpha4) || Input.GetKey(KeyCode.Keypad4) || Input.GetKey(KeyCode.F))
				AddHeroToStock(HeroType.hero4);

			if (Input.GetKey(KeyCode.Alpha5) || Input.GetKey(KeyCode.Keypad5) || Input.GetKey(KeyCode.DownArrow))
				AddHeroToStock(HeroType.hero5);

			if (Input.GetKey(KeyCode.Alpha6) || Input.GetKey(KeyCode.Keypad6) || Input.GetKey(KeyCode.RightArrow))
				AddHeroToStock(HeroType.hero6);

			if (Input.GetKey(KeyCode.Space))
				LaunchHeroes();
		}

		if (Input.GetKeyDown(KeyCode.Escape))
			Application.Quit();
	}

	public void spawnEnemy()
	{
		int r = Random.Range(0, System.Enum.GetValues(typeof(EnemyType)).Length);
		EnemyType e = (EnemyType)System.Enum.GetValues(typeof(EnemyType)).GetValue(r);

		EnemyUI m = Instantiate(monsterPrefab).GetComponent<EnemyUI>();

		m.transform.position = enemySpawnPoint.position;
		m.transform.parent = enemySpawnPoint;
		m.game = this;
		m.dir = monsterDir;

		m.setMonster(getMonster(e));

		soundManager.AddMonster(m);
	}

	void AddHeroToStock(HeroType hero)
	{
		if (!_heroStock.Contains(hero) && (Time.time - _heroBuffer[hero]) > bufferTime)
		{
			if(_heroStock.Count == 0)
			{
				HeroUI h = Instantiate(heroPrefab).GetComponent<HeroUI>();
				h.transform.position = heroSpawnPoint.position;
				h.transform.parent = heroSpawnPoint;
				h.game = this;
				_currentHeros = h;
			}

			GenerateJonesName();
			_heroStock.Add(hero);
			Hero heroInfo = getHero(hero);

			_currentHeros.AddHero(heroInfo);
			float phraseLength = _currentHeros.PlayCatchPhrase();

			//Animation
			heroShowSprite.sprite = heroInfo.icon;
			heroShowText.text = heroInfo.name;
			heroShowText.font = heroInfo.font;
			MeshRenderer rend = heroShowText.GetComponentInChildren<MeshRenderer>();
			rend.material = heroInfo.font.material;
			heroShowBackground.enabled  = true;

			heroShowAnimator.SetTrigger("Show");

			musicMixer.GetFloat("music_volume", out initialMusicVolume);
			StartCoroutine(SetMixer("music_volume", initialMusicVolume + musicVolumeDifferenceAtPhrase));

			StartCoroutine(SetPause(phraseLength));
		}
		_heroBuffer[hero] = Time.time;
	}

	void LaunchHeroes()
	{
		if(_heroStock.Count != 0 && (Time.time - _launchBuffer) > bufferTime)
		{
			_currentHeros.Go();
			_heroStock.Clear();
		}
		_launchBuffer = Time.time;
	}

	bool containsMonster(EnemyType type)
	{
		foreach(Monster m in monsterList)
		{
			if (m.type == type)
				return true;
		}

		return false;
	}

	bool containsHero(HeroType type)
	{
		foreach (Hero m in heroList)
		{
			if (m.type == type)
				return true;
		}

		return false;
	}

	public Hero getHero(HeroType type)
	{
		foreach (Hero m in heroList)
		{
			if (m.type == type)
				return m;
		}
		return null;
	}

	Monster getMonster(EnemyType type)
	{
		foreach (Monster m in monsterList)
		{
			if (m.type == type)
				return m;
		}
		return null;
	}

	private void OnDrawGizmosSelected()
	{
		if (monsterList.Count < System.Enum.GetValues(typeof(EnemyType)).Length)
		{
			foreach(EnemyType e in System.Enum.GetValues(typeof(EnemyType)))
			{
				if (!containsMonster(e))
					monsterList.Add(new Monster(e));
			}
		}

		if (heroList.Count < System.Enum.GetValues(typeof(HeroType)).Length)
		{
			foreach (HeroType e in System.Enum.GetValues(typeof(HeroType)))
			{
				if (!containsHero(e))
					heroList.Add(new Hero(e));
			}
		}
	}

	/// <summary>
	/// Si ça retourne true, hero gagne, sinon false
	/// </summary>
	/// <param name="heros"></param>
	/// <param name="monster"></param>
	/// <returns></returns>
	public bool Fight(List<HeroType> heros, EnemyType monster)
	{
		foreach(HeroType e in heros)
		{
			if (getHero(e).winAgainst.Contains(monster))
				return true;
		}

		return false;
	}

	public void Lose()
	{
		Debug.Log("Lose");
		PlayerPrefs.SetInt("finalScore", (int)_score);
		PlayerPrefs.SetFloat("finalTime", _gameTime);
		SceneManager.LoadScene(2);
	}

	public int IncrementScore(float value)
	{
		_score += value * ((int)(_gameTime / 60f) + 1) * 100;

		return (int)(value * ((int)(_gameTime / 60f) + 1) * 100);
	}

	IEnumerator SetPause(float time)
	{
		if(!Paused)
		{
			TogglePause();

			yield return new WaitForSecondsRealtime(time);

			TogglePause();
			heroShowBackground.enabled = false;
			heroShowAnimator.SetTrigger("FinishEntryShow");
			StartCoroutine(SetMixer("music_volume", initialMusicVolume));
		}
	}

	IEnumerator SetMixer(string mixer, float value)
	{
		float start = Time.unscaledTime;
		float startValue = value;
		musicMixer.GetFloat(mixer, out startValue);

		while(Time.unscaledTime - start < musicFadeDuration)
		{
			musicMixer.SetFloat(mixer, Mathf.Lerp(startValue, value, (Time.unscaledTime - start) / musicFadeDuration));
			yield return null;
		}

		musicMixer.SetFloat(mixer, value);
	}

	private void TogglePause()
	{
		if (Paused)
		{
			Paused = false;
			Time.timeScale = 1f;
		}
		else
		{
			Paused = true;
			Time.timeScale = 0f;
		}
	}
}
