﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EnemyVarManager : MonoBehaviour {

    public float enemySpeed = 2.0f;
    public Vector3 enemyDirection = Vector3.left;

    public List<GameObject> childEnemies = new List<GameObject>();
}
