﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LooseScreenManager : MonoBehaviour {

	public Text finalTime = null;
	public Text finalscore = null;

	// Use this for initialization
	void Start ()
	{
		float gameTime = PlayerPrefs.GetFloat("finalTime");
		float minutes = Mathf.Floor(gameTime / 60);
		float seconds = Mathf.RoundToInt(gameTime % 60);

		finalTime.text = string.Format("{0:00}:{1:00}", minutes, seconds); ;
		finalscore.text =  PlayerPrefs.GetInt("finalScore").ToString();
	}
	
	// Update is called once per frame
	void Update ()
	{
	}

	public void GoToMainMenu()
	{
		SceneManager.LoadScene(0);
	}
}
