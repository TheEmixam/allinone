﻿using UnityEngine;
using System.Collections;

public class EnemyUI : MonoBehaviour {
	public SpriteRenderer sprite;
	public float yAmplification;
	public float force;

	[HideInInspector]
    public EnemyType enemyType = 0;
	[HideInInspector]
	public Vector3 dir;
	[HideInInspector]
	public Game game;

	public static Vector3 StartPoint;
	public static Vector3 EndPoint;
	public static float DistanceEndStart;
	public static SoundManager SoundManagerInstance;

	private AudioSource aSource;

	public float ProgressRatio
	{
		get
		{
			return (transform.position - StartPoint).magnitude / DistanceEndStart;
		}
	}

	private Rigidbody2D _rigidbody;
	private Collider2D _collider;

	private void Awake()
	{
		_rigidbody = GetComponent<Rigidbody2D>();
		_collider = GetComponent<Collider2D>();
		aSource = GetComponent<AudioSource>();
	}

	private void Start()
	{
		aSource.Play();
	}

	private void Update()
    {
		if(_rigidbody.isKinematic)
			transform.Translate(dir * game.CurrentSpeed * Time.deltaTime);

		if (transform.position.y < game.limitDestroy.position.y)
		{
			SoundManagerInstance.RemoveMonster(this);
			Destroy(gameObject);
		}
    }

	private void OnDrawGizmosSelected()
	{
		Debug.DrawLine(transform.position, transform.position + new Vector3(-1, yAmplification).normalized * 3);
		Debug.DrawLine(transform.position, transform.position + new Vector3(1, yAmplification).normalized * 3);
	}

	void OnTriggerEnter2D (Collider2D col)
    {
        HeroUI hero = col.GetComponent<HeroUI>();
        if (hero != null)
        {
			game.spawnStars((transform.position + hero.transform.position) * .5f);

			if (game.Fight(hero.heroes, enemyType))
			{
				Die();
				hero.Success();
			}
			else
				hero.Die();
        }
    }

	public void setMonster(Monster m)
	{
		enemyType = m.type;
		sprite.sprite = m.icon;
		aSource.clip = m.spawnAudioClip;
		_rigidbody.centerOfMass = new Vector2(0, m.centerY);
	}

	void Die()
	{
		_collider.enabled = false;
		_rigidbody.isKinematic = false;

		Vector3 f = new Vector2(Random.Range(0, 1f), yAmplification).normalized;
		_rigidbody.AddForce(f * force, ForceMode2D.Impulse);
		_rigidbody.AddTorque(f.x * force, ForceMode2D.Impulse);
	}
}
