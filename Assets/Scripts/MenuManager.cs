﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;


public class MenuManager : MonoBehaviour {

	public Text playerNumberText = null;
	private int playerNumber = 2;

	// Use this for initialization
	void Start ()
	{
		playerNumber = PlayerPrefs.GetInt("playerNumber", 2);
	}
	
	// Update is called once per frame
	void Update ()
	{
		playerNumberText.text = playerNumber.ToString();

		if (Input.GetKeyDown(KeyCode.Escape))
			Application.Quit();
	}

	public void Play()
	{
		SceneManager.LoadScene(1);// = main scene
	}
	public void IncreasePlayerNulber()
	{
		playerNumber++;
		playerNumber = Mathf.Clamp(playerNumber, 1, 4);
		PlayerPrefs.SetInt("playerNumber", playerNumber);
	}
	public void DecreasePlayerNumber()
	{
		playerNumber--;
		playerNumber = Mathf.Clamp(playerNumber, 1, 4);
		PlayerPrefs.SetInt("playerNumber", playerNumber);
	}

}
