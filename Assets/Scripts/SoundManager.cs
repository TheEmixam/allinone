﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour {

	public audio_script audioScript;

	[HideInInspector]
	public List<EnemyUI> monsterList = new List<EnemyUI>();

	private AudioSource aSource;

	private void Start()
	{
		aSource = GetComponent<AudioSource>();
	}

	private void Update()
	{
		float maxValue = 0;

		foreach(EnemyUI e in monsterList)
		{
			maxValue = Mathf.Max(maxValue, e.ProgressRatio);
		}

		if (maxValue < 0.33f)
			audioScript.setMusic(1);
		else if (maxValue < 0.666f)
			audioScript.setMusic(2);
		else
			audioScript.setMusic(3);
	}

	public void AddMonster(EnemyUI enemy)
	{
		monsterList.Add(enemy);
	}

	public void RemoveMonster(EnemyUI enemy)
	{
		if (monsterList.Contains(enemy))
			monsterList.Remove(enemy);
	}
}
