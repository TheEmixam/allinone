﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class HeroUI : MonoBehaviour {

	public List<SpriteRenderer> sprites = new List<SpriteRenderer>();
	public TextMesh comboText;

    public List<HeroType> heroes = new List<HeroType>();

	[Header("Force")]
	public float yAmplification;
	public float force;

	[HideInInspector]
	public Game game;
	private Collider2D _collider;
	private bool _go = false;

	private int combo = 0;

	public ParticleSystem runParticles = null;

	[HideInInspector]
	public List<AudioClip> catchPhrases = new List<AudioClip>();
	public AudioClip dieSound;
	public AudioClip runSound;

	private AudioSource aSource;

	private float _xForce;
	private float _maxXForce;
	private float _acceleration = 6;

	private void Awake()
	{
		_collider = GetComponent<Collider2D>();
		comboText.gameObject.SetActive(false);
		aSource = GetComponent<AudioSource>();
		_maxXForce = 6;
	}

	public void Brake()
	{
		_xForce -= _xForce * 1.5f;
	}

	private void Update()
	{
		if (_go)
		{
			_xForce = Mathf.Min(_maxXForce, _xForce + _acceleration * Time.deltaTime);

			transform.Translate(new Vector3(_xForce * Time.deltaTime, 0));

			if (transform.position.x > game.limitDestroy.position.x)
				Destroy(gameObject);
		}

		bool destroy = true;
		foreach (SpriteRenderer sprite in sprites)
		{
			if (sprite != null)
			{
				if (sprite.transform.position.y < game.limitDestroy.position.y)
					Destroy(sprite.gameObject);

				destroy = false;
			}
		}

		if (destroy)
			Destroy(gameObject);
	}

	private void OnDrawGizmosSelected()
	{
		Debug.DrawLine(transform.position, transform.position + new Vector3(-1, yAmplification).normalized * 3);
		Debug.DrawLine(transform.position, transform.position + new Vector3(1, yAmplification).normalized * 3);
	}

	public void Go()
    {
		aSource.clip = runSound;
		aSource.Play();

		_collider.enabled = true;
		_go = true;
		runParticles.Play();
	}

	public float PlayCatchPhrase()
	{
		aSource.clip = catchPhrases[Random.Range(0, catchPhrases.Count)];
		aSource.Play();
		return aSource.clip.length;
	}

	public void AddHero(Hero h)
	{
		heroes.Add(h.type);

		for(int i = 0; i < heroes.Count - 1; i++)
		{
			int invert = heroes.Count - (i + 1);
			sprites[invert].sprite = sprites[invert - 1].sprite;
			sprites[invert].GetComponent<Rigidbody2D>().centerOfMass = sprites[invert - 1].GetComponent<Rigidbody2D>().centerOfMass;
		}

		Rigidbody2D rigidbody = sprites[0].GetComponent<Rigidbody2D>();
		rigidbody.centerOfMass = new Vector2(0, h.centerY);
		sprites[0].sprite = h.icon;
		catchPhrases = h.catchPhrases;
	}

	public void Success()
	{
		Brake();

		int s = game.IncrementScore(++combo);

		if(combo == 1)
			comboText.gameObject.SetActive(true);

		comboText.text = "+" + s;
	}

	public void Die()
	{
		aSource.clip = dieSound;
		aSource.Play();

		_collider.enabled = false;
		comboText.gameObject.SetActive(false);
		_go = false;
		runParticles.Stop();
		foreach(SpriteRenderer sprite in sprites)
		{
			Rigidbody2D rigidbody = sprite.GetComponent<Rigidbody2D>();

			rigidbody.isKinematic = false;

			Vector3 f = new Vector2(Random.Range(-1f, 0), yAmplification).normalized;
			rigidbody.AddForce(f * force, ForceMode2D.Impulse);
			rigidbody.AddTorque(f.x * force, ForceMode2D.Impulse);
		}
	}
}
