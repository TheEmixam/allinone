﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameTitle : MonoBehaviour {

	private TextMesh titleTextMesh;
	private MeshRenderer titleMeshRenderer;
	public List<Font> fonts;

	// Use this for initialization
	void Start () {
		titleTextMesh = GetComponent<TextMesh>();
		titleMeshRenderer = GetComponent<MeshRenderer>();
		StartCoroutine(SwitchFont());
	}

	IEnumerator SwitchFont()
	{
		while (true)
		{
			Font randomFont = fonts[Random.Range(0, fonts.Count)];
			if (randomFont == titleTextMesh.font)
				continue;

			titleTextMesh.font = randomFont;
			titleMeshRenderer.material = randomFont.material;
			yield return new WaitForSeconds(1f);
		}
	}
}
