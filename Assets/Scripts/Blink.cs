﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Blink : MonoBehaviour {

	public float blinkSpeed;
	public Sprite sprite1;
	public Sprite sprite2;

	private SpriteRenderer _renderer;

	private void Start()
	{
		_renderer = GetComponent<SpriteRenderer>();

		StartCoroutine(blinkCoroutine());
	}

	IEnumerator blinkCoroutine()
	{
		while(true)
		{
			_renderer.sprite = sprite1;
			yield return new WaitForSecondsRealtime(blinkSpeed);

			_renderer.sprite = sprite2;

			yield return new WaitForSecondsRealtime(blinkSpeed);
		}
	}


}
