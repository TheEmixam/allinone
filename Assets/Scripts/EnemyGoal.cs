﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyGoal : MonoBehaviour {

	public Game game;

	private void OnTriggerEnter2D(Collider2D collision)
	{
		EnemyUI e = collision.GetComponent<EnemyUI>();

		if(e != null)
		{
			game.Lose();
		}
	}
}
