﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class audio_script : MonoBehaviour {


	public AudioMixer audio_mixer;

	public bool channel_1;
	public bool channel_2;
	public bool channel_3;
	public bool stop;
	public bool alternateChannel3;


	public float fadeout_speed = 10.0f;
	public float fadein_speed = 30.0f;

	private AudioSource audio_channel_1;
	private AudioSource audio_channel_2;
	private AudioSource audio_channel_3;




	private float audio_channel_1_vol = -80.0f;
	private float audio_channel_2_vol = -80.0f;
	private float audio_channel_3_vol = -80.0f;


	private Object[] AudioArray_channel_1;
	private Object[] AudioArray_channel_2;
	private Object[] AudioArray_channel_3;




	// Use this for initialization
	void Start () {

		audio_channel_1 = (AudioSource)gameObject.AddComponent <AudioSource>();
		audio_channel_2 = (AudioSource)gameObject.AddComponent <AudioSource>();
		audio_channel_3 = (AudioSource)gameObject.AddComponent <AudioSource>();


		AudioArray_channel_1 = Resources.LoadAll("1",typeof(AudioClip));
		AudioArray_channel_2 = Resources.LoadAll("2",typeof(AudioClip));
		AudioArray_channel_3 = Resources.LoadAll("3",typeof(AudioClip));


		audio_channel_1.outputAudioMixerGroup = audio_mixer.FindMatchingGroups("channel_1")[0];
		audio_channel_2.outputAudioMixerGroup = audio_mixer.FindMatchingGroups("channel_2")[0];
		audio_channel_3.outputAudioMixerGroup = audio_mixer.FindMatchingGroups("channel_3")[0];


		audio_channel_1.clip = AudioArray_channel_1[0] as AudioClip;
		audio_channel_2.clip = AudioArray_channel_2[0] as AudioClip;
		if (alternateChannel3)
			audio_channel_3.clip = AudioArray_channel_3[1] as AudioClip;
		else
			audio_channel_3.clip = AudioArray_channel_3[0] as AudioClip;

		audio_channel_1.loop = true;
		audio_channel_2.loop = true;
		audio_channel_3.loop = true;


		audio_channel_1.Play();
		audio_channel_2.Play();
		audio_channel_3.Play();


	}
	
	// Update is called once per frame
	void Update () {
		SetVolumes ();
		if (stop) {
			StopAllMusic ();
		}

	}

	public void StopAllMusic(){
		channel_1 = false;
		channel_2 = false;
		channel_3 = false;

		if (audio_channel_1_vol < -79.0f) {
			audio_channel_1.Stop ();
		}
		if (audio_channel_2_vol < -79.0f) {
			audio_channel_2.Stop ();
		}
		if (audio_channel_3_vol < -79.0f) {
			audio_channel_3.Stop ();
		}


	}

	public void SetVolumes(){
		audio_mixer.SetFloat ("channel_1", audio_channel_1_vol);
		audio_mixer.SetFloat ("channel_2", audio_channel_2_vol);
		audio_mixer.SetFloat ("channel_3", audio_channel_3_vol);


		if (channel_1) {
			if (audio_channel_1_vol < 0.0f) {
				audio_channel_1_vol += fadein_speed * Time.deltaTime;	
			}
		}
		if (!channel_1) {
			if (audio_channel_2_vol > -30.0f | audio_channel_3_vol > -30.0f) {
				if (audio_channel_1_vol > -80.0f) {
					audio_channel_1_vol -= fadeout_speed * Time.deltaTime;	
				}
			}

		}

		if (channel_2) {
			if (audio_channel_2_vol < 0.0f) {
				audio_channel_2_vol += fadein_speed * Time.deltaTime;	
			}
		}
		if (!channel_2) {
			if (audio_channel_1_vol > -30.0f | audio_channel_3_vol > -30.0f) {
				if (audio_channel_2_vol > -80.0f) {
					audio_channel_2_vol -= fadeout_speed * Time.deltaTime;	
				}
			}

		}
		if (channel_3) {
			if (audio_channel_3_vol < 0.0f) {
				audio_channel_3_vol += fadein_speed * Time.deltaTime;	
			}
		}
		if (!channel_3) {
			if (audio_channel_1_vol > -30.0f | audio_channel_2_vol > -30.0f) {
				if (audio_channel_3_vol > -80.0f) {
					audio_channel_3_vol -= fadeout_speed * Time.deltaTime;	
				}
			}

		}


	}

	public void StopMusic(){
		stop = true;
	}

	public void PlaySunAndMoon(){
		channel_1 = true;
		channel_2 = false;
		channel_3 = false;
	}
		
	public void PlaySun(){
		channel_1 = false;
		channel_2 = true;
		channel_3 = false;
	}
	public void PlayMoon(){
		channel_1 = false;
		channel_2 = false;
		channel_3 = true;
	}


	public void setMusic(int id)
	{
		switch (id)
		{
			case 1:
				channel_1 = true;
				channel_2 = false;
				channel_3 = false;
				break;
			case 2:
				channel_1 = false;
				channel_2 = true;
				channel_3 = false;
				break;
			case 3:
				channel_1 = false;
				channel_2 = false;
				channel_3 = true;
				break;
			default:
				break;
		}
	}
}
